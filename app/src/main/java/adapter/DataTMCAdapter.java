package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mikola11.shameapplication1.R;

import java.util.List;

import pojo.DataTermoMetricCharacter;

/**
 * Created by mikola11 on 27.05.15.
 */
public class DataTMCAdapter extends BaseAdapter {

    private List<DataTermoMetricCharacter> list;
    private LayoutInflater layoutInflater;


    public DataTMCAdapter(Context context, List<DataTermoMetricCharacter> list) {
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {//повертає кількість елементів

        return list.size();
    }

    @Override
    public Object getItem(int position) {//повертає елемент

        return list.get(position);
    }

    @Override

    public long getItemId(int position) {//повертає позицію елемента

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view==null){
            view = layoutInflater.inflate(R.layout.item_layout, parent, false);
        }

        DataTermoMetricCharacter dataTermoMetricCharacter = getDataTermoMetricCharacter(position);

        TextView txtTemperature = (TextView) view.findViewById(R.id.txtTemperature);
        TextView txtCurrent = (TextView) view.findViewById(R.id.txtCurrent);
        txtTemperature.setText(dataTermoMetricCharacter.getTemperature());
        txtCurrent.setText(dataTermoMetricCharacter.getCurrent());

        return view;
    }

    private DataTermoMetricCharacter getDataTermoMetricCharacter(int position){
        return (DataTermoMetricCharacter) getItem(position);
    }
}
