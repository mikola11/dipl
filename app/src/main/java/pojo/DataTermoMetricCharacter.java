package pojo;

/**
 * Created by mikola11 on 27.05.15.
 */
public class DataTermoMetricCharacter {

    private long id;
    private String temperature;
    private String current;

    public DataTermoMetricCharacter(long id, String temperature, String current) {
        this.id = id;
        this.temperature = temperature;
        this.current = current;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }
}
