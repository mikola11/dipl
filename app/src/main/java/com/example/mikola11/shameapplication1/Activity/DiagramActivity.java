package com.example.mikola11.shameapplication1.Activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.LinearLayout;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;


import com.example.mikola11.shameapplication1.R;

/**
 * Created by mikola11 on 03.06.15.
 */
public class DiagramActivity extends Activity {

    private Double[] temperatureDoubleArray = {1.4, 1.6, 1.8, 2.00, 2.20, 2.40, 2.60, 2.80, 3.00, 3.20, 3.40, 3.60, 3.80, 4.00, 4.20, 4.40, 4.60, 4.80, 5.00, 5.50, 6.00, 6.50, 7.00, 7.50, 8.00, 8.50, 9.00, 9.50, 10.00, 10.50, 11.00, 11.50, 12.00, 12.50, 13.00, 13.50, 14.00, 14.50, 15.00, 15.50, 16.00, 16.50, 17.00, 17.50, 18.00, 18.50, 19.00, 19.50, 20.00, 21.00, 22.00, 23.00, 24.00, 25.00, 26.00, 27.00, 28.00, 29.00, 30.00, 32.00, 34.00, 36.00, 38.00, 40.00, 42.00, 44.00, 46.00, 48.00, 50.00, 52.00, 54.00, 56.00, 58.00, 60.00, 65.00, 70.00, 75.00, 77.35, 80.00, 85.00, 90.00, 95.00, 100.00, 110.00, 120.00, 130.00, 140.00, 150.00, 160.00, 170.00, 180.00, 190.00, 200.00, 210.00, 220.00, 230.00, 240.00, 250.00, 260.00, 270.00, 273.15, 280.00, 290.00, 300.00, 305.00, 310.00, 320.00, 330.00, 340.00, 350.00, 360.00, 370.00, 380.00, 390.00, 400.00, 410.00, 420.00, 430.00, 440.00, 450.00, 460.00, 470.00, 475.00};
    private Double[] currentDoubleArray = {1.69812, 1.69521, 1.69177, 1.68786, 1.68352, 1.6788, 1.67376, 1.66845, 1.66292, 1.65721, 1.65134, 1.64529, 1.63905, 1.63263, 1.62602, 1.6192, 1.6122, 1.60506, 1.59782, 1.57928, 1.56027, 1.54097, 1.52166, 1.50272, 1.48443, 1.467, 1.45048, 1.43488, 1.42013, 1.40615, 1.39287, 1.38021, 1.36809, 1.35647, 1.3453, 1.33453, 1.32412, 1.31403, 1.30422, 1.29464, 1.28527, 1.27607, 1.26702, 1.2581, 1.24928, 1.24053, 1.23184, 1.22314, 1.2144, 1.19645, 1.17705, 1.15558, 1.13598, 1.12463, 1.11896, 1.11517, 1.11212, 1.10945, 1.10702, 1.10263, 1.09864, 1.0949, 1.09131, 1.08781, 1.08436, 1.08093, 1.07748, 1.07402, 1.07053, 1.067, 1.06346, 1.05988, 1.05629, 1.05267, 1.04353, 1.03425, 1.02482, 1.02032, 1.01525, 1.00552, 0.99565, 0.98564, 0.9755, 0.95487, 0.93383, 0.91243, 0.89072, 0.86873, 0.8465, 0.82404, 0.80138, 0.77855, 0.75554, 0.73238, 0.70908, 0.68564, 0.66208, 0.63841, 0.61465, 0.5908, 0.58327, 0.5669, 0.54294, 0.51892, 0.50688, 0.49484, 0.47069, 0.44647, 0.42221, 0.39783, 0.37337, 0.34881, 0.32416, 0.29941, 0.27456, 0.24963, 0.22463, 0.19961, 0.17464, 0.14985, 0.12547, 0.10191, 0.09062};

    private GraphicalView mChart;
    private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
    private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
    private XYSeries mCurrentSeries;
    private XYSeriesRenderer mCurrentRenderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diagram_layout);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout);
        if (mChart == null) {
            initChart();
            addSampleData();
            mChart = ChartFactory.getCubeLineChartView(this, mDataset, mRenderer, 0.3f);
            layout.addView(mChart);
        } else {
            mChart.repaint();
        }
    }

    private void initChart() {
        mCurrentSeries = new XYSeries("Графік апроксимації ТМХ діодного сенсора температури");
        mDataset.addSeries(mCurrentSeries);
        mCurrentRenderer = new XYSeriesRenderer();
        mRenderer.addSeriesRenderer(mCurrentRenderer);
        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setBackgroundColor(Color.WHITE);
        mRenderer.setMarginsColor(Color.GRAY);
        mRenderer.setLabelsTextSize(25);
        mRenderer.setXLabels(20);
        mRenderer.setYLabels(20);
        mRenderer.setLabelsColor(Color.BLACK);
//        mRenderer.setChartValuesTextSize(20);
        mRenderer.setFitLegend(true);
//        mRenderer.setLegendHeight(2);
        mRenderer.setLegendTextSize(19);
//        mRenderer.setXTitle("Температура,K");
//        mRenderer.setYTitle("Напруга,мB");

    }

    private void addSampleData() {

        for (int i=0; i<=122; i++){

            Double t = temperatureDoubleArray[i];
            Double c = currentDoubleArray[i];
            mCurrentSeries.add(t,c);
        }
        }
}
